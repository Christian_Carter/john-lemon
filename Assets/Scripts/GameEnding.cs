﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public GameObject canvasObject;
    public GameObject canvasObject2;
    public GameObject canvasObject3;
    public GameObject canvasObject4;

    public GameObject EmptyStars;
    public GameObject half;
    public GameObject star;
    public GameObject starandhalf;
    public GameObject twostars;
    public GameObject twostarsandhalf;
    public GameObject threestars;

    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public Level1Timer timer;
    public float points;
    public float remainder;
    public float endingtimer = 10f;
    public float ghostkilled = 0;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI secondsText;


    bool m_IsPlayerAtExit;
    public bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;
    
    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }

    public void CaughtPlayer ()
    {
        m_IsPlayerCaught = true;
    }

    void Update ()
    {
        if (m_IsPlayerAtExit)
        {
            EndLevel (exitBackgroundImageCanvasGroup, false, exitAudio);
            timer.Stop = true;
            endingtimer -= 1 * Time.deltaTime;
            print(endingtimer);
            if (endingtimer <= 0)
            {
                //Application.Quit ();   - quits the game when its in .exe format.
                //SceneManager.LoadScene (0);       - restarts the scene or the game.
                //UnityEditor.EditorApplication.isPlaying = false;         - quits the game when its in editor mode.
            }
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel (caughtBackgroundImageCanvasGroup, true, caughtAudio);
            timer.Stop = true;
            endingtimer -= 1 * Time.deltaTime;
            if (endingtimer <= 0)
            {
                SceneManager.LoadScene (0);
                UnityEditor.EditorApplication.isPlaying = false;
            }
        }

    }

    void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
            
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            points = timer.currentTime * 10;
            if (m_IsPlayerCaught == true)
            {
                canvasObject4.SetActive(false);
                EmptyStars.SetActive(true);
                canvasObject3.SetActive(false);
            }
            else
            {
                canvasObject.SetActive(true);
                canvasObject2.SetActive(false);
                canvasObject3.SetActive(false);
                
                scoreText.text = "Score: " + points.ToString("0");
                remainder = timer.startingTime - timer.currentTime;
                secondsText.text = "You beat the level in " + remainder.ToString("0");
                // 17 seconds is the fastest time i got playing the level
                if (points >= 145 && points <= 200)
                {
                    threestars.SetActive(true);
                }
                else if (points >= 130 && points < 144)
                {
                    twostarsandhalf.SetActive(true);
                }
                else if (points >= 105 && points < 130)
                {
                    twostars.SetActive(true);
                }
                else if (points >= 80 && points < 105)
                {
                    starandhalf.SetActive(true);
                }
                else if (points >= 50 && points < 80)
                {
                    star.SetActive(true);
                }
                else if (points >= 5 && points < 50)
                {
                    half.SetActive(true);
                }
            }
        }
    }
}